# Event Viewer Front End SPA

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.

## Getting Started

### Prerequisites

* Node JS 8.12.0 or newer version
* NPM 6.4.1 or newer version
* angular/cli 7.3.5
* Docker 19.03

### Buiding

Create a folder in you local file system, 

```
mkdir project
cd project
```

Git clone this project to project folder.

```
git clone git@gitlab.com:chuanbao.lu/event-viewer.git
```

Enter event-viewer folder.

```
cd event-viewer
```

## Start The Application

You can start the application using Angular CLI in development mode or using docker to build docker image and container, then run docker container.

### Use Angular CLI to start the appliction

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build docker image

Run `npm run docker_build` to create a docker image 'events-viewer'.  Run `docker images` to check if 'events-viewer' is created.

### Create a docker container and run the container

After docker image 'events-viewer' is created, run `npm run docker_run`.  Navigate to `http://localhost/`

## Authors

**Chuanbao Lu** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

