import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { EventService } from './event.service';
import * as moment from 'moment';

fdescribe('EventServiceService', () => {
  let httpMock: HttpTestingController;
  let service: EventService;

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EventService],
    })
  );

  it('should be created', () => {
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(EventService);
    expect(service).toBeTruthy();
  });

  it('test next week', () => {
    service = TestBed.get(EventService);
    const startDate = moment('20100101', 'YYYYMMDD').toDate();
    const endDate = service.calcOtherDate(startDate, 'NEXT', 'WEEK');
    expect(endDate).toEqual(moment('20100108', 'YYYYMMDD').toDate());
  });

  it('test next month', () => {
    service = TestBed.get(EventService);
    const startDate = moment('20100101', 'YYYYMMDD').toDate();
    const endDate = service.calcOtherDate(startDate, 'NEXT', 'MONTH');
    expect(endDate).toEqual(moment('20100201', 'YYYYMMDD').toDate());
  });

  it('test next quarter', () => {
    service = TestBed.get(EventService);
    const startDate = moment('20100101', 'YYYYMMDD').toDate();
    const endDate = service.calcOtherDate(startDate, 'NEXT', 'QUARTER');
    expect(endDate).toEqual(moment('2010-04-01', 'YYYY-MM-DD').toDate());
  });

  it('test prev week', () => {
    service = TestBed.get(EventService);
    const startDate = moment('20100608', 'YYYYMMDD').toDate();
    const endDate = service.calcOtherDate(startDate, 'PREV', 'WEEK');
    expect(endDate).toEqual(moment('20100601', 'YYYYMMDD').toDate());
  });

  it('test prev month', () => {
    service = TestBed.get(EventService);
    const startDate = moment('20100601', 'YYYYMMDD').toDate();
    const endDate = service.calcOtherDate(startDate, 'PREV', 'MONTH');
    expect(endDate).toEqual(moment('20100501', 'YYYYMMDD').toDate());
  });

  it('test prev quarter', () => {
    service = TestBed.get(EventService);
    const startDate = moment('20100601', 'YYYYMMDD').toDate();
    const endDate = service.calcOtherDate(startDate, 'PREV', 'QUARTER');
    expect(endDate).toEqual(moment('20100301', 'YYYYMMDD').toDate());
  });
});
