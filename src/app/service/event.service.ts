import { EEvent } from './../model/event';
import { EventSearchResult } from '../model/event-search-result';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const DATE_PATTERN = 'YYYYMMDD';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  constructor(private http: HttpClient) {}

  findEvents(startDate: Date, endDate: Date): Observable<EventSearchResult> {
    const startMoment = moment(startDate);
    const endMoment = moment(endDate);
    const url =
      environment.baseApiPath +
      '/event?start=' +
      (startMoment.isValid() ? startMoment.format(DATE_PATTERN) : '') +
      '&end=' +
      (endMoment.isValid() ? endMoment.format(DATE_PATTERN) : '');
    return this.http.get<EventSearchResult>(url);
  }

  findEventDetails(id: number): Observable<EEvent> {
    const url = environment.baseApiPath + '/event/' + id;
    return this.http.get<EEvent>(url);
  }

  findEarliestEventDate(): Observable<Date> {
    const url = environment.baseApiPath + '/earliest-event-date';
    return this.http.get<string>(url).pipe(map(strDate => moment(strDate, DATE_PATTERN).toDate()));
  }

  calcOtherDate(startDate: Date, searchType: string, range: string): Date {
    const startMoment = moment(startDate);
    const amount = searchType === 'NEXT' ? 1 : -1;
    let endMoment: moment.Moment;
    switch (range) {
      case 'WEEK':
        endMoment = startMoment.add(amount, 'week');
        break;
      case 'MONTH':
        endMoment = startMoment.add(amount, 'month');
        break;
      case 'QUARTER':
        endMoment = startMoment.add(amount * 3, 'month');
        break;
    }
    return endMoment.toDate();
  }
}
