import { EEvent } from './event';
export class EventSearchResult {
  startDate: Date;
  endDate: Date;
  events: EEvent[];

  constructor(
    options: {
      startDate?: Date;
      endDate?: Date;
      events?: EEvent[];
    } = {}
  ) {
    this.startDate = options.startDate;
    this.endDate = options.endDate;
    this.events = options.events;
  }
}
