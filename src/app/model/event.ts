export class EEvent {
  id: number;
  event_date: Date;
  event_type: string;
  event_size: number;
  event_summary: string;
  event_details: string;

  constructor(
    options: {
      id?: number;
      event_date?: Date;
      event_type?: string;
      event_size?: number;
      event_summary?: string;
      event_details?: string;
    } = {}
  ) {
    this.id = options.id;
    this.event_date = options.event_date;
    this.event_type = options.event_type;
    this.event_size = options.event_size;
    this.event_summary = options.event_summary;
    this.event_details = options.event_details;
  }
}
