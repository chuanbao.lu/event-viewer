import { EventSearchResult } from '../../model/event-search-result';
import { EEvent } from './../../model/event';
import { EventService } from './../../service/event.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-event-search',
  templateUrl: './event-search.component.html',
  styleUrls: ['./event-search.component.css'],
})
export class EventSearchComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private eventService: EventService,
    private modalService: BsModalService
  ) {}

  formGroup: FormGroup;
  result: EventSearchResult;
  selectedEvent: EEvent;
  error: string;
  isLoading = false;

  pageContent: EEvent[];
  currentPage = 0;

  hasNewStartDate = true;

  modalRef: BsModalRef;

  @ViewChild('modalTemplate')
  modalTemplateRef: TemplateRef<any>;

  ngOnInit() {
    this.initForm();
  }

  searchOptionChanged() {
    this.hasNewStartDate = true;
  }

  get totalOfEvents() {
    if (this.result && this.result.events) {
      return this.result.events.length;
    } else {
      return 0;
    }
  }

  private initForm() {
    this.formGroup = this.formBuilder.group({
      startDate: new FormControl(moment().toDate()),
      searchType: new FormControl('NEXT'),
      dateRange: new FormControl('WEEK'),
    });
    this.eventService.findEarliestEventDate().subscribe(d => {
      this.formGroup.controls.startDate.setValue(d);
      this.searchEvents();
    });
  }

  private setStartDateWhenContinueSearch() {
    const searchType = this.formGroup.controls.searchType.value;
    if (!this.hasNewStartDate) {
      if (this.result) {
        if (searchType === 'NEXT') {
          this.formGroup.controls.startDate.setValue(this.result.endDate);
        } else {
          this.formGroup.controls.startDate.setValue(this.result.startDate);
        }
      }
    }
  }

  searchEvents() {
    this.isLoading = true;
    this.setStartDateWhenContinueSearch();
    this.error = null;
    let startDate = this.formGroup.controls.startDate.value;
    const searchType = this.formGroup.controls.searchType.value;
    let endDate = this.eventService.calcOtherDate(startDate, searchType, this.formGroup.controls.dateRange.value);
    if (searchType === 'PREV') {
      const tmp = startDate;
      startDate = endDate;
      endDate = tmp;
      this.formGroup.controls.startDate.setValue(startDate);
    }
    this.eventService.findEvents(startDate, endDate).subscribe({
      next: (result: EventSearchResult) => {
        this.hasNewStartDate = false;
        this.result = result;
        this.getFirstPage();
        this.isLoading = false;
      },
      error: err => {
        this.error = 'Can not find any event. Error: ' + err;
        this.isLoading = false;
      },
    });
  }

  private getFirstPage() {
    this.pageContent = this.result.events.slice(0, 25);
  }

  pageChanged(event: PageChangedEvent): void {
    const startIndex = (event.page - 1) * event.itemsPerPage;
    const endIndex = event.page * event.itemsPerPage;
    this.pageContent = this.result.events.slice(startIndex, endIndex);
  }

  showDetails(eventId: number) {
    this.eventService.findEventDetails(eventId).subscribe({
      next: e => (this.selectedEvent = e),
      error: error => (this.error = this.error),
    });
    this.openModal();
  }

  openModal() {
    this.modalRef = this.modalService.show(this.modalTemplateRef, { class: 'large-dialog' });
  }

  get hasEvents(): boolean {
    return this.result && this.result.events && this.result.events.length > 0;
  }
}
